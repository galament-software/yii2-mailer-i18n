<?php
/**
 * @copyright (c) 2014, Galament
 * @author Petrov Aleksanr <burnb83@gmail.com>
 * Date: 16.07.14
 * Time: 10:20
 */
namespace burn\mailerI18n;

class MailListTranslateKeys
{
    /**
     * List of translate keys for mail subject and body(with params)
     * need for inserting translation keys to db by console command "yii message"
     * Copy in project folder, there scan "yii message" command and fill it
     * by pair keys body, subject.
     * As shown in the example below
     */
    public function keyList()
    {
        /**
         * Example:
         *
         * Yii::t('email', 'layout footer');
         *
         * Yii::t('email', 'subject customer_activated');
         * Yii::t('email', 'body customer_activated_{fullName}{password}{customerId}');
         *
         * Yii::t('email', 'subject customer_activated_admin');
         * Yii::t('email', 'body customer_activated_admin_{fullName}{password}{customerId}');
         *
         */
    }
}