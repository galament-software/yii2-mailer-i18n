<?php
/**
 * @copyright (c) 2014, Galament
 * @author Petrov Aleksanr <burnb83@gmail.com>
 * Date: 16.07.14
 * Time: 14:14
 */

namespace burn\mailerI18n;


/**
 * Interface CustomerSettingsInterface
 * @package burn\mailerI18n
 */
interface CustomerSettingsInterface
{
    /**
     * @return string
     */
    public function getLanguage();
} 