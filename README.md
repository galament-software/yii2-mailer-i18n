Yii2 Mailer i18n
===================
------------------
What is this?
-------------

Class MailerI18n needed for send email, with message internationalization body and subject.
Message language may selected by parameter language in models implements CustomerSettingsInterface
Message body may insert parameters array, for dynamic content

------------------

Installation
------------

Yii2 framework and PHP 5.4 or higher is required to use it.

Installation is recommended to be done via [composer][] by adding the following to the `require` section in your `composer.json`:

```json
"burn/yii2-mailer-i18n": "dev-master"
```

Run `composer update` afterwards.

------------------

Usage
-----
### In your PHP project

To send mail, you just create an instance of MailerI18n with required params
> - string $to Email where send
> - string $type Type of message, involved in translate key
> - CustomerSettingsInterface $customerSettings
> - array $params Dynamic params for body message, involved in translate key

and call the `send()`- method:

```php
$customerSettings = CustomerSettings::findOne(['customer_id' => Yii::$app->user->id]);
$params = ['{customerId}' => Yii::$app->user->id];

$mailer = new MailerI18n('test@example.com', 'customer_activated', $customerSettings, $params);
$mailer->send();
```

>For success work yii message generator you can copy mail-list-translate-keys/MailListTranslateKeys.php in project folder,
>there scan "yii message" command and fill it by pair keys body, subject. As shown in the example in this file.

#####
> Also you can modify "from email", category in Yii::t() and layout for message by changing model MailerI18n property
> - $from,
> - $tCategory,
> - $layout