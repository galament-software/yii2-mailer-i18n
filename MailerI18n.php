<?php
/**
 * User: Burn
 * Date: 15.07.14
 * Time: 12:01
 */
namespace burn\mailerI18n;

use yii\mail\BaseMailer;
use Yii;


/**
 * Class MailerI18n
 * @package burn\mailerI18n
 */
class MailerI18n
{
    /**
     * @var string - Category in Yii::t()
     */
    public $tCategory = 'email';
    /**
     * @var string - Email from send
     */
    public $from = 'support@example.com';
    /**
     * @var string - Layout for message
     */
    public $layout = '@burn/mailerI18n/layouts/html';
    /**
     * @var string - Email where send
     */
    public $to;
    /**
     * @var string - Type of message, involved in translate key
     */
    private $_type;
    /**
     * @var array - Dynamic params for body message, involved in translate key
     */
    private $_params;

    /**
     * @var string - Language for message and subject, sets from $customerSettings instance of ActiveRecordInterface field language
     */
    private $_language = null;


    /**
     * @param string $to - Email where send
     * @param string $type - Type of message, involved in translate key
     * @param CustomerSettingsInterface $customerSettings
     * @param array $params - Dynamic params for body message, involved in translate key
     */
    public function __construct($to, $type, $params, $customerSettings = null)
    {
        if (Yii::$app->params['supportEmail']) {
            $this->from = Yii::$app->params['supportEmail'];
        }
        $this->to = $to;
        $this->_type = $type;
        $this->_params = $params;
        if ($customerSettings instanceof CustomerSettingsInterface) {
            $this->_language = $customerSettings->getLanguage();
        }
    }

    /**
     * Set language of message and send message
     * @return bool
     */
    public function send()
    {
        if ($this->to) {
            /** @var $mailer BaseMailer */
            $mailer = Yii::$app->mailer;

            return $mailer
                ->compose()
                ->setFrom($this->from)
                ->setTo($this->to)
                ->setSubject($this->getSubject())
                ->setHtmlBody($mailer->render($this->layout, ['content' => $this->getBody(), 'tCategory' => $this->tCategory]))
                ->send();
        }
        return false;
    }

    /**
     * Get mail subject
     * @return string
     */
    public function getSubject()
    {
        $subjectTranslateKey = 'subject_' . $this->_type;
        return Yii::t($this->tCategory, $subjectTranslateKey, [], $this->_language);
    }

    /**
     * Get mail body
     * @return mixed
     */
    public function getBody()
    {
        $bodyTranslateKey = 'body_' . $this->_type . $this->serializeParamsForKeys();
        /**
         * @var string $body - Html body for message
         */
        $body = str_replace(array_keys($this->_params), array_values($this->_params), Yii::t($this->tCategory, $bodyTranslateKey, [], $this->_language));
        return $body;
    }

    /**
     * Return serialize params keys for involve translate key
     * @return string
     */
    private function serializeParamsForKeys()
    {
        $stringParams = '';
        if (is_array($this->_params)) {
            ksort($this->_params);
            foreach ($this->_params as $param => $value) {
                $stringParams .= $param;
            }
        }
        return $stringParams;
    }
} 